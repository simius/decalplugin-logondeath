# LogOnDeath (Lod)  
  
Very simple, GUI-less, decal plugin that listens to the Death event.  
When the player dies, it will output some information then wait (set number) of minutes and log out.  
The logout can be canceled from commandline.  
  
## General Info.   
 
* Current version: 1.0.0.0  
* Report issues in the issue tracker.  
* Issues will be fixed if there is time, no promices.  
* Please read license before downloading source or plugin .dlls.
  
## Install Instructions.  
 
1. Place LogOnDeath.dll where you wish to store it (your plugins folder is probably a good place).  
2. Open Decal and press 'Add'.  
3. In Add/Remove Plugins dialog, Press 'Browse'.  
4. Locate the LogOnDeath.dll file and select it, press 'Save'.  
5. Close Add/Remove plugins dialog, check so that 'LogOnDeath.PluginCore' is active in the Decal Agent.  
6. Run AC.

## Uninstall Instructions  

1. Open Decal.
2. Select LogOnDeath.PluginCore
3. Press 'Remove'.
4. Delete LogOnDeath.dll
  

## License & legal  
  
	THE SOFTWARE IS PROVIDED "AS IS" AND THE AUTHOR DISCLAIMS ALL 
	WARRANTIES WITH REGARD TO THIS SOFTWARE INCLUDING ALL
	IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS. IN NO EVENT SHALL 
	THE AUTHOR BE LIABLE FOR ANY SPECIAL,
	DIRECT, INDIRECT, OR CONSEQUENTIAL DAMAGES OR ANY DAMAGES WHATSOEVER 
	RESULTING FROM LOSS OF USE, DATA OR PROFITS,
	WHETHER IN AN ACTION OF CONTRACT, NEGLIGENCE OR OTHER TORTIOUS ACTION, 
	ARISING OUT OF OR IN CONNECTION WITH THE USE
	OR PERFORMANCE OF THIS SOFTWARE. 

----  
  
  
LogOnDeath is Licensed under a Creative Commons Attribution-NonCommercial 3.0 Unported License.  
For more info:  
[http://creativecommons.org/licenses/by-nc/3.0/deed.en_US](http://creativecommons.org/licenses/by-nc/3.0/deed.en_US)  