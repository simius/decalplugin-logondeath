using System;
using System.Windows.Forms;
using Decal.Adapter;

namespace LogOnDeath
{
	public partial class PluginCore : PluginBase
	{
		#region Variables.
		private bool isLogging = false;
		private bool isRunning = true;
		private Timer logOutTimer = null;
		#endregion

		#region Events.

		private void TimerTick(object sender, EventArgs e)
		{
			logOutTimer.Stop();
			if (isLogging && isRunning)
			{
				WriteToChat("One minute passed without canceling. Logging out.", 5);
				Core.Actions.Logout();
			}
		}

		private void Death(object sender, Decal.Adapter.Wrappers.DeathEventArgs e)
		{
			if (!isRunning || isLogging)
				return;
			isLogging = true;
			WriteToChat("You have died. LogOnDeath will now wait one minute, then if not canceled, log you out.", 5);
			WriteToChat("To Cancel, write /lodstop in chat.", 5);
			logOutTimer.Start();
		}

		private void LoginComplete(object sender, EventArgs e)
		{
			WriteToChat("LogOnDeath initialized. Write /lodhelp for commands.", 5);
		}

		private void ChatCommand(object sender, ChatParserInterceptEventArgs e)
		{
			if (e.Text[0] == '/')
			{
				string command = e.Text.Substring(1);
				if (command.StartsWith("lod"))
				{
					command = command.Substring(3).ToUpper();
					switch (command)
					{
						case "HELP":
							PrintHelp();
							break;
						case "STOP":
							StopLogout();
							break;
						case "ON":
							SetState(true);
							break;
						case "OFF":
							SetState(false);
							break;
					}
				}
			}
		}
		#endregion

		#region Overrides.
		protected override void Startup()
		{
			try
			{
				Core.CharacterFilter.LoginComplete += new EventHandler(LoginComplete);
				Core.CharacterFilter.Death += new EventHandler<Decal.Adapter.Wrappers.DeathEventArgs>(Death);
				Core.CommandLineText += new EventHandler<ChatParserInterceptEventArgs>(ChatCommand);
				logOutTimer = new Timer();
				logOutTimer.Tick += new EventHandler(TimerTick);
				logOutTimer.Interval = 60 * 1000;
			}
			catch (Exception ex)
			{
				DebugOutput("Error: " + ex.Message);
			}
		}

		protected override void Shutdown()
		{
			try
			{
			}
			catch (Exception ex)
			{
				DebugOutput("Error: " + ex.Message);
			}
		}
		#endregion

		#region Commands.

		private void PrintHelp()
		{
			WriteToChat("Available commands:", 5);
			WriteToChat("/lodhelp - This command list.", 5);
			WriteToChat("/lodoff - stops the plugin.", 5);
			WriteToChat("/lodon - starts the plugin if its stopped.", 5);
			WriteToChat("/lodstop - stops the logout process.", 5);
		}

		private void StopLogout()
		{
			if (isLogging)
			{
				WriteToChat("Log out timer have been stopped.", 5);
				isLogging = false;
				if (logOutTimer != null)
					logOutTimer.Stop();
			}
		}

		private void SetState(bool val)
		{
			isRunning = val;
			if (val)
				WriteToChat("LogOnDeath is now turned on.", 5);
			else
				WriteToChat("LogOnDeath is now turned off. Type /lodon to turn it back on.", 5);
		}
		#endregion

		private void WriteToChat(string msg, int color)
		{
			try
			{
				Core.Actions.AddChatText(msg, color, 1);
			}
			catch (Exception ex)
			{
				DebugOutput("Error: " + ex.Message);
			}
		}

		private void DebugOutput(string output)
		{ 
#if DEBUG
			Core.Actions.AddChatText(output, 3, 1);
#endif
			Core.Actions.AddStatusText(output);
		}

	}
}